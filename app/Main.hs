module Main where

import           Lib

collatz :: Int -> String
collatz x = "" ++ show b ++ ":" ++ show a
 where
  a = collatz' [x]
  b = length a - 1

collatz' :: [Int] -> [Int]
collatz' (x : xs) | x == 1    = x : xs
                  | even x    = collatz' $ x `div` 2 : x : xs
                  | otherwise = collatz' $ (3 * x) + 1 : x : xs

main :: IO ()
main =
    -- should be 111
  putStrLn $ collatz 27
